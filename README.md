# Pulse-App

Aplicação utilizada para testar a API do Spotify e para aplicar novos conceitos de Arquitetura e conhecimentos de Angular.

## Tecnologias utilizadas:

- `Angular 8`
- `Node js 10`
- `Sass`
- `Api do Spotify retirada daqui:` [Spotify](https://developer.spotify.com)
